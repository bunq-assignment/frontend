import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestGuard, AuthGuard } from './core/guards';

const routes: Routes = [
  { path: 'auth', loadChildren: './modules/auth/auth.module#AuthModule', canActivate: [GuestGuard] },
  { path: 'chat', loadChildren: './modules/chat/chat.module#ChatModule', canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'chat' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
