import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonApiModule } from 'angular2-jsonapi';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JsonApiModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    JsonApiModule
  ]
})
export class SharedModule { }
