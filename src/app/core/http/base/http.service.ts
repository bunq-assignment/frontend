import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    protected interceptsErrors = false;

    constructor(protected http?: HttpClient) { }


    Interceptable() {
        this.interceptsErrors = true;
        return this;
    }

    protected getOptions() {
        const options: any = {};
        if (this.interceptsErrors) {
            options.headers = {
                'X-Intercept-Errors': 'true'
            };
        }
        this.interceptsErrors = false;
        return options;
    }
}
