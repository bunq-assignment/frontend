import { Injectable } from '@angular/core';
import { HttpService } from './base/http.service';
import { CONFIG } from '../config';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends HttpService{

  private baseUrl = CONFIG.API.BASE_URL + CONFIG.API.AUTH;

  public register(email: string, password: string, password_confirmation: string, name: string) {
    return this.http.post(this.baseUrl + '/register', { email, password, password_confirmation, name });
  }

  public login(email: string, password: string) {
    return this.http.post(this.baseUrl + '/login', { email, password });
  }

  public logout() {
    return this.http.post(this.baseUrl + '/logout', {});
  }
}
