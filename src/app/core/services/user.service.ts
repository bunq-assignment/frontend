import { Injectable } from '@angular/core';
import { CONFIG } from '../config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  get token(): string {
    return localStorage.getItem(CONFIG.STORAGE.AUTH_TOKEN_KEY);
  }

  set token(value: string) {
    localStorage.setItem(CONFIG.STORAGE.AUTH_TOKEN_KEY, value);
  }

  get user(): {} {
    return JSON.parse(localStorage.getItem(CONFIG.STORAGE.USER_KEY));
  }

  set user(value: {}) {
    localStorage.setItem(CONFIG.STORAGE.USER_KEY, JSON.stringify(value));
  }

  clear() {
    localStorage.removeItem(CONFIG.STORAGE.AUTH_TOKEN_KEY);
    localStorage.removeItem(CONFIG.STORAGE.USER_KEY);
  }
}
