import { environment } from '../../environments/environment';

export const CONFIG = {
    API: {
        BASE_URL: environment.API_BASE_URL || '/api',
        AUTH: '/auth'
    },
    STORAGE: {
        AUTH_TOKEN_KEY: 'access_token',
        AUTH_TOKEN_EXPIRES_AT_KEY: 'token_expires_at',
        USER_KEY: 'user',
    },
    REDIRECTS: {
        AUTH: '/',
        GUEST: '/login'
    },
};
