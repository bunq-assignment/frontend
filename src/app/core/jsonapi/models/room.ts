import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { Message } from './';
import { User } from './user';

@JsonApiModelConfig({
    type: 'rooms'
})
export class Room extends JsonApiModel {

    @Attribute()
    title: string;

    @Attribute({ serializedName: 'created_at' })
    createdAt: Date;

    @Attribute({ serializedName: 'updated_at' })
    updatedAt: Date;

    @HasMany()
    messages: Message[];

    @HasMany()
    users: User[];
}
