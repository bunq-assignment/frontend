import { JsonApiModelConfig, JsonApiModel, Attribute, BelongsTo } from 'angular2-jsonapi';
import { Room, User } from './';


@JsonApiModelConfig({
    type: 'messages'
})
export class Message extends JsonApiModel {

    @Attribute()
    content: string;

    @Attribute({ serializedName: 'room_id' })
    roomId: string;

    @Attribute({ serializedName: 'user_id' })
    user_id: string;

    @Attribute({ serializedName: 'created_at' })
    createdAt: Date;

    @Attribute({ serializedName: 'updated_at' })
    updatedAt: Date;

    @BelongsTo()
    room: Room;

    @BelongsTo()
    user: User;
}
