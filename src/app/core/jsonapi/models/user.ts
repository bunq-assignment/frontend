import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany } from 'angular2-jsonapi';
import { Room, Message } from './';


@JsonApiModelConfig({
    type: 'users'
})
export class User extends JsonApiModel {

    @Attribute()
    email: string;

    @Attribute()
    name: string;

    @Attribute({ serializedName: 'created_at' })
    createdAt: Date;

    @Attribute({ serializedName: 'updated_at' })
    updatedAt: Date;

    @HasMany()
    room: Room;

    @HasMany()
    user: Message;
}
