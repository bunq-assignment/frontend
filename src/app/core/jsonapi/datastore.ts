import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonApiDatastoreConfig, JsonApiDatastore, DatastoreConfig } from 'angular2-jsonapi';
import { CONFIG } from '../config';
import { Room, Message, User } from './models';

const config: DatastoreConfig = {
  baseUrl: CONFIG.API.BASE_URL,
  models: {
    rooms: Room,
    messages: Message,
    users: User
  }
};

@Injectable({
  providedIn: 'root'
})
@JsonApiDatastoreConfig(config)
export class Datastore extends JsonApiDatastore {

    constructor(http: HttpClient) {
        super(http);
    }

}
