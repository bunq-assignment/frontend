import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JsonApiHeadersInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headers = {};
        // set X-Requested-With if value not set beforehand
        if (!request.headers.has('X-Requested-With')) {
            headers['X-Requested-With'] = 'XMLHttpRequest';
        }
        // set Content-Type if value not set beforehandor or if the request type is FormData
        if (!request.headers.has('Content-Type') && !(request.body instanceof FormData)) {
            headers['Content-Type'] = 'application/json';
        }
        // set Accept if value not set beforehand
        if (!request.headers.has('Accept')) {
            headers['Accept'] = 'application/json';
        }
        request = request.clone({
            setHeaders: headers
        });

        return next.handle(request);
    }
}
