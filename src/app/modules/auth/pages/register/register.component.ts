import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/http/auth.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup = this.fb.group({
    email: [null, [Validators.email]],
    password: [null, [Validators.required]],
    password_confirmation: [null, [Validators.required]],
    name: [null, [Validators.required]],
  });

  errors: string[] = [];

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.errors = [];
    this.authService.register(this.email, this.password, this.passwordConfirmation, this.name)
    .subscribe((successResponse: HttpResponse<any>) => {
      this.router.navigate(['/auth/login']);
    }, (errorResponse: HttpErrorResponse) => {
      if (errorResponse.status === 422) {
        Object.values(errorResponse.error.errors).forEach((value: string[]) => {
          this.errors.push(...value);
        });
      } else {
        this.errors = ['Something went wrong.'];
      }
    });
  }

  get email() { return this.registerForm.controls['email'].value; }

  get password() { return this.registerForm.controls['password'].value; }

  get passwordConfirmation() { return this.registerForm.controls['password_confirmation'].value; }

  get name() { return this.registerForm.controls['name'].value; }

}
