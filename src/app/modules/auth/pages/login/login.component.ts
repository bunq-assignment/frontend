import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../../core/http/auth.service';
import { Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    email: [null, [Validators.email]],
    password: [null, [Validators.required]],
  });

  errors: string[] = [];

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.errors = [];
    this.authService.login(this.email, this.password)
    .subscribe((successResponse: any) => {
      console.log(successResponse);
      this.userService.token = successResponse.access_token;
      this.userService.user = successResponse.user;
      this.router.navigate(['/chat']);
    }, (errorResponse: HttpErrorResponse) => {
      if (errorResponse.status === 422) {
        Object.values(errorResponse.error.errors).forEach((value: string[]) => {
          this.errors.push(...value);
        });
      } else if (errorResponse.status === 401) {
        this.errors = ['The credentials you\'ve entered do not match.'];
      } else {
        this.errors = ['Something went wrong.'];
      }
    });
  }

  get email() { return this.loginForm.controls['email'].value; }

  get password() { return this.loginForm.controls['password'].value; }



}
