import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { Datastore } from '../../../../core/jsonapi/datastore';
import { Room, User, Message } from '../../../../core/jsonapi/models';
import { UserService } from '../../../../core/services/user.service';
import { AuthService } from '../../../../core/http/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {

  messageForm: FormGroup = this.fb.group({
    content: [null, [Validators.required]],
    room_id: [null, [Validators.required]]
  });

  errors: string[] = [];

  rooms: Room[] = [];

  users: User[] = [];

  currentRoom: Room = null;

  messagePollingInterval: any;

  constructor(
    private fb: FormBuilder,
    private datastore: Datastore,
    public userService: UserService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.datastore.findAll(Room, { include: 'users,messages' })
      .subscribe((rooms: JsonApiQueryData<Room>) => {
        this.rooms = rooms.getModels();
        this.currentRoom = this.rooms[0] || null;
        if (this.currentRoom) {
          this.messageForm.controls['room_id'].setValue(this.currentRoom.id);
        }
      });

    this.datastore.findAll(User)
      .subscribe((users: JsonApiQueryData<User>) => {
        this.users = users.getModels().filter((user: User) => {
          return user.id !== this.userService.user['id'];
        });
      });

      this.messagePollingInterval = setInterval(() => {
        if (this.currentRoom) {
          this.datastore.findRecord(Room, this.currentRoom.id, { include: 'messages' })
          .subscribe((newRoom: Room) => {
            for (const room of this.rooms) {
              if (room.id === newRoom.id) {
                room.messages = newRoom.messages;
              }
            }
          });
        }
      }, 2000);
  }

  ngOnDestroy() {
    clearInterval(this.messagePollingInterval);
  }

  addFriend(user: User) {
    const room: Room = this.datastore.createRecord(Room, {
      title: '---',
      users: [user, new User(this.datastore, this.userService.user)]
    });

    room.save().subscribe((newRoom: Room) => {
      this.rooms.push(newRoom);
      this.currentRoom = newRoom;
      this.messageForm.controls['room_id'].setValue(this.currentRoom.id);
    }, (error: any) => {
      this.errors = ['Something went wrong.'];
    });
  }

  changeRoom(room: Room) {
    this.currentRoom = room;
    this.messageForm.controls['room_id'].setValue(room.id);
  }

  onSubmitMessage() {
    if (this.messageForm.valid) {
      const message: Message = this.datastore.createRecord(Message, {
        content: this.messageForm.controls['content'].value,
        roomId: this.messageForm.controls['room_id'].value,
      });

      message.save().subscribe((newMessage: Message) => {
        this.messageForm.controls['content'].setValue(null);
        for (const room of this.rooms) {
          if (room.id === newMessage.roomId) {
            if (!this.currentRoom.messages) {
              this.currentRoom.messages = [];
            }
            this.currentRoom.messages.push(newMessage);
            break;
          }
        }
      }, (error: any) => {
        this.errors = ['Something went wrong.'];
      });
    }
  }

  logout() {
    this.authService.logout().subscribe((successResponse: any) => {
      this.userService.clear();
      this.router.navigate(['/auth']);
    }, (errorResponse: HttpErrorResponse) => {
      this.errors = ['Something went wrong.'];
    });
  }

  getRoomName(room: Room): string {
    for (const user of room.users) {
      if (user.id !== this.userService.user['id']) {
        return user.name;
      }
    }
    return null;
  }
}
